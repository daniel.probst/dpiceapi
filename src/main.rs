use chrono::{DateTime, Utc};
use getopts::Options;
use serde_derive::Deserialize;
use std::cmp;
use std::collections::HashMap;
use std::env;

// struct to hold data from api
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct IceContract {
    market_id: u64,
    last_time: Option<String>,
    last_price: Option<f64>,
    change: f64,
    volume: u64,
    market_strip: String,
    end_date: String,
}

// trying to add print traits to options
// dont know how to do it generically
pub trait OptPrint {
    fn opt_print(&self) -> String;
}
impl OptPrint for Option<String> {
    fn opt_print(&self) -> String {
        match self {
            Some(x) => format!("{}", x),
            None => "NA".to_string(),
        }
    }
}
impl OptPrint for Option<f64> {
    fn opt_print(&self) -> String {
        match self {
            Some(x) => format!("{}", x),
            None => "NA".to_string(),
        }
    }
}

// different printing methods
impl IceContract {
    fn print_contract(&self, style: &ContractPrintStyle) {
        match style {
            ContractPrintStyle::Price => println!("{}", self.last_price.opt_print()),
            ContractPrintStyle::StripPrice => {
                println!("{} {}", self.market_strip, self.last_price.opt_print());
            }
            ContractPrintStyle::OneLiner => {
                println!(
                    "{} {} {:?}",
                    self.market_strip,
                    self.last_price.opt_print(),
                    self.last_time
                );
            }
        }
    }
}

// struct to hold data of different contract codes
#[derive(Debug)]
struct IceCodeDesc {
    code: String,
    desc: String,
    product_id: usize,
    hub_id: usize,
}

// selector enum to cli select printing style
enum ContractPrintStyle {
    Price,
    StripPrice,
    OneLiner,
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn print_contracts(x: &HashMap<String, IceCodeDesc>) {
    println!("Valid implemented contracts are");
    for (key, value) in x {
        println!("{}: {:?}", key, value);
    }
}

fn iceurl_gcfj(pid: usize, hib: usize) -> String {
    format!("https://www.theice.com/marketdata/DelayedMarkets.shtml?getContractsAsJson=&productId={}&hubId={}",pid,hib)
}

fn main() {
    // define different implemented contracts and their master data
    // todo: pack this elswhere
    let mut contract_lookup = HashMap::new();
    let icecode_tfm: IceCodeDesc = IceCodeDesc {
        code: "TFM".to_string(),
        desc: "ICE TTF Gase Futures".to_string(),
        product_id: 4331,
        hub_id: 7979,
    };
    contract_lookup.insert(icecode_tfm.code.clone(), icecode_tfm);
    let icecode_b: IceCodeDesc = IceCodeDesc {
        code: "B".to_string(),
        desc: "ICE Brent Crude Futures".to_string(),
        product_id: 254,
        hub_id: 403,
    };
    contract_lookup.insert(icecode_b.code.clone(), icecode_b);
    // end of master data hashmap

    // getopts
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    // Define options using crate getopts https://docs.rs/getopts/
    let mut opts = Options::new();
    opts.optopt("c", "contract", "set contract type", "CONTRACT");
    opts.optflag("d", "debug", "print debug information");
    opts.optflag("h", "help", "print this help menu");
    opts.optflagopt("n", "", "number of contract tenors", "T");
    opts.optflag("p", "print", "print available CONTRACTs");
    opts.optflagopt("v", "", "verbosity level", "V");
    // get them from cmd line
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };
    // print help and exit
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    // print available contract and exit
    if matches.opt_present("p") {
        print_contracts(&contract_lookup);
        return;
    }
    // print some debugging stuff
    if matches.opt_present("d") {
        // todo print local time contract and stuff
        let now: DateTime<Utc> = Utc::now();
        println!("UTC now is: {}", now);
    }

    // construct ICE url
    let contract_choice = matches
        .opt_get_default("c", "B".to_string())
        .unwrap()
        .to_uppercase();
    let contract_struct = contract_lookup.get(&contract_choice);
    if contract_struct.is_none() {
        print_contracts(&contract_lookup);
        return;
    }
    let url = iceurl_gcfj(
        contract_struct.unwrap().product_id,
        contract_struct.unwrap().hub_id,
    );
    // get url response
    // todo refactor unwrap
    let body = reqwest::blocking::get(&url).unwrap().text().unwrap();
    // deserialize json
    let ice_contracts: Vec<IceContract> = serde_json::from_str(&body).unwrap();
    if matches.opt_present("d") {
        println!("response body = {:?}", ice_contracts);
    }
    // compare request # tenors  to received
    let n_tenors_req = matches.opt_get_default("n", 1usize).unwrap();
    let n_tenors = cmp::min(n_tenors_req, ice_contracts.len());
    if matches.opt_present("d") && n_tenors < n_tenors_req {
        println!(
            "Requested {} tenors, but received only {}",
            n_tenors_req, n_tenors
        );
    }
    // check printstyle
    let printstyle_req = matches.opt_get_default("v", 1usize).unwrap();
    let printstyle: ContractPrintStyle = match printstyle_req {
        3 => ContractPrintStyle::OneLiner,
        2 => ContractPrintStyle::StripPrice,
        _ => ContractPrintStyle::Price,
    };
    // print contract
    // refactor into fn taking contract vec, number, printstyle
    for i in 0..n_tenors {
        ice_contracts[i].print_contract(&printstyle)
    }
}
